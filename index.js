const resultBox = document.getElementById("results");
const buttons = document.querySelectorAll("button");

buttons.forEach((button) => {
    button.addEventListener("click", (event) => {
        const buttonNew = event.target.textContent;
        if (buttonNew === "AC") {
            resultBox.value = "0";
        }
        else if (buttonNew === "DEL") {
            resultBox.value = resultBox.value.substring(0, resultBox.value.length - 1);
        }
        else if (!resultBox.value.includes(".") || buttonNew !== ".") {
            if (!resultBox.value.includes(".") && parseFloat(resultBox.value) === 0) {
                resultBox.value = buttonNew;
            }
            else {
                resultBox.value += buttonNew;
            }
        }
        if (resultBox.value === "") {
            resultBox.value = "0";
        }
        if (resultBox.value === ".") {
            resultBox.value = "0."
        }
    });
});